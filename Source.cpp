#include <iostream>
#include <string>
int main()
{
    std::string str = "Hello Skillbox";
    std::cout << str.length() << "\n";
    char firstCharacter = str[0];
    std::cout << firstCharacter << "\n";
    std::cout << str.back() << "\n";
    return 0;
}